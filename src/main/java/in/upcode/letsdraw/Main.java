package in.upcode.letsdraw;

import in.upcode.letsdraw.model.*;
import in.upcode.letsdraw.model.Drawable;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.printf("Hello wonderful!! \n\"The only way to do great work is to love what you do.\" - Steve Jobs%n");

        System.out.println("Enter te number of lines to print patterns : ");
        Scanner scanner = new Scanner(System.in);
        System.out.println();

        int numberOfLines = scanner.nextInt();
        List<Drawable> patterns = List.of(
                new DiamondPattern(numberOfLines));

        for (Drawable pattern : patterns) {
            pattern.draw();
        }

    }
}
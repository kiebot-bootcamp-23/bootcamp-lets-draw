package in.upcode.letsdraw.model;

public class RightTrianglePattern implements Drawable {
    private int numberOfLines;

    public RightTrianglePattern(int numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    @Override
    public void draw() {
        System.out.println("Draw the given pattern: " +
                           "\n*\n**\n***\n****\n*****");

        throw new RuntimeException("Draw the above pattern!!");
    }
}

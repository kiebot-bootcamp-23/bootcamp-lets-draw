package in.upcode.letsdraw.model;

public class RectanglePattern implements Drawable {
    private int numberOfLines;

    public RectanglePattern(int numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    @Override
    public void draw() {
        System.out.println("Draw the given pattern: " +
                           "\n******\n******\n******\n******\n******");

        throw new RuntimeException("Draw the above pattern!!");
    }
}

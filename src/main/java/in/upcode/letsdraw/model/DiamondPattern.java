package in.upcode.letsdraw.model;

public class DiamondPattern implements Drawable {
    private int numberOfLines;

    public DiamondPattern(int numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    @Override
    public void draw() {
        System.out.printf("Draw the given pattern: \n    *\n   * *\n  * * *\n * * * *\n* * * * *\n * * * *\n  * * *\n   * *\n    *\n%n");
        throw new RuntimeException("Draw the above pattern!!");
    }
}

package in.upcode.letsdraw.model;

public interface Drawable {
    public void draw();
}

# Lets Draw

### How to Run
Run the application from IDE.

**OR**

Run the application from terminal


For Mac `./gradlew run`

For Windows `gradlew.bat run`

### What to do

Follow the instructions from [Here](https://kiebot.notion.site/Art-of-drawing-aa4b3f3196224d07a02009a9160275c4)